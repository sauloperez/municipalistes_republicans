![logo](logo.jpg)

## D’on venim i on anem?

Aquest projecte parteix  de la necessitat de bastir candidatures àmplies i unitàries on les bases dels espais socials i polítics ja existents, o en procés d’estructuració, i que es situen en contra del Règim del 36-78 es vegin reflectides i se sentin còmodes. **Reconeixem, necessitem i posem en valor els espais polítics ja existents fins al moment.**

És per això que, partint d’aquesta realitat, tothom qui en participi també podrà formar part del present projecte. L’objectiu no és substituir-ne cap sinó adaptar-los(nos) a cada context per, no només **construir espais guanyadors arrabassant el màxim d’alcaldies possibles al bloc del 155 i treure pes a la sociovergència arreu del territori, sinó aprofundir políticament i social en la crisi regeneradora** oberta arran del referèndum d’autodeterminació de l’1-O.

Ens dirigim al gruix de les classes populars catalanes i volem **construir polítiques de recuperació de la sobirania en tots els aspectes vitals i comunitaris**, de manera coordinada i enxarxada entre municipis. Tenim la voluntat política de guanyar les institucions i posar-les al servei de la majoria, convertint-les en instruments per distribuir el poder.

Agafar, des del municipalisme, la responsabilitat d’organitzar i liderar el procés constituent i així continuar sumant sectors al procés de profunda repolitització i apoderament que viu la societat catalana. Per construir una República des de baix. O més d’una.

## Valors

**Transversalitat i pluralitat**: som un moviment sociopolític divers, ampli, plural i de base de l’esquerra transformadora que fa frontissa i aposta per construir majories republicanes amb voluntat de governar.

**Municipalisme**: Calen noves maneres de fer polítiques. Noves respostes per a una societat que ha canviat. Entenem els municipis com a espais de sobirania directa en interdependència i cooperació amb altres municipis i regions i reivindiquem eines efectives que potenciïn polítiques per combatre les desigualtats i la precarietat.

**Defensa dels drets socials fonamentals**: Defensem amb fermesa tots els drets socials i la seva garantia efectiva. Entenem el feminisme com a eix transversal per combatre el racisme i el feixisme.

**Radicalitat democràtica**: Cal fomentar una democràcia directa, oberta i realment participativa. La transparència i el control ciutadà han de permetre l’accés directe i garantir l’accés a la informació i el coneixement, la participació en la deliberació, en la presa de decisions, en la seva execució i en el seguiment d’aquesta.

**Assemblea i política oberta**: Treballem en espais polítics assemblearis on s’hi participa a títol personal i on les organitzacions polítiques donen suport però garantint l’autonomia de l’assemblea i comprometent-se a no interferir en les seves dinàmiques.

**Sobirania, autodeterminació i República Catalana**: Aquest espai és emancipador i no subjecte ni supeditat a velles receptes i aposta decididament per l’exercici de l’autodeterminació i la sobirania efectiva. Entenem el republicanisme com a una eina per la igualtat, justícia social, feminisme, antifeixisme i antiracisme, transformació dels models de municipi, procés constituent des de baix.

## Objectius polítics i programa de mínims

### Drets socials fonamentals

Les nostres viles i ciutats han estat durament castigades per la crisi i per enormes desigualtats generades per l’actual sistema econòmic. Però, alhora, són un gresol riquíssim d’iniciatives ciutadanes valentes que treballen per **pal·liar les injustícies** i per mantenir cohesionada la societat d’una manera generosa i eficaç.

**L’objectiu central doncs de l’acció política és que tota la ciutadania tingui una vida digna**, apostant per les polítiques feministes com a clau superadora i transformadora. Cal treballar de manera urgent plans d’emergència per a les persones més afectades, per a les classes populars.

**Combatrem l’atur** des de l’àmbit municipal amb mesures urgents i imaginatives. La remunicipalització de serveis suposarà, entre d’altres, una eina molt important en aquesta lluita. Calen mecanismes per blindar els avenços en la remunicipalització de serveis i evitar-ne una regressió posterior en forma de reprivatització. Cal que la titularitat pública es combini amb el control popular a través de cooperatives de consum o formes de gestió comunitària.

Desplegarem **polítiques de seguretat valentes**, demostrant que tenim alternativa al model hegemònic actual basat en la por i la repressió.

Un altre front serà **evitar els desnonaments i garantir un habitatge digne a totes les persones, alhora que garantir l’atenció a les persones en situació d’exclusió social**, a les quals proporcionarem acompanyament, suport personalitzat i assessorament legal. Per posar en pràctica aquestes iniciatives, crearem un fons contra l’exclusió social i la pobresa (alimentària, energètica…) com a mesura de xoc.

### Governar obeint: radicalitat democràtica, participació i drets de ciutadania

Per avançar de forma efectiva en l’apoderament popular, cal **desplegar polítiques de participació valentes**, a nivell municipal i també de forma col·legiada i col·lectiva entre municipis.

Des dels municipis hem d’encapçalar les propostes per a defensar-nos dels atacs als drets civils bàsics fonamentals de tota la ciutadania, vingui d’on vingui. La **cohesió social** és fonamental per a combatre el feixisme i racisme creixents, així com per construir pobles i ciutats amb les persones al centre de l’acció política.

### Per un canvi de model econòmic: igualtat és justícia

És imprescindible **transformar l’actual model socioeconòmic** apostant per una economia justa, social i solidària, lliure de combustibles fòssils, de proximitat, ecològica i feminista. I per això són necessàries mesures com revertir les privatitzacions i el desmantellament dels serveis públics, **augmentar la despesa social**, seguir apostant per mecanismes efectius i existents de redistribució de la riquesa a nivell municipal o tendir a la sobirania alimentària i energètica.

Calen polítiques que incentivin un **desenvolupament social i humà a les nostres localitats** que defugin de les lògiques consumistes, apostin per una clara prevenció de residus i en democratitzin la seva gestió i tinguin un compromís ferm cap a la lluita i adaptació al canvi climàtic.

Hem de posar les eines adequades per a poder **desenvolupar un model de desenvolupament econòmic i comercial**, local i territorial, que permeti avançar en la transformació del model actual.

Així mateix, s’ha de diversificar l’activitat econòmica i **abandonar l’actual procés de terciarització aguda** que està abocant a les classes populars a uns altíssims nivells de precarització, **donant suport a les lluites laborals**.

El **transport públic com a eina estratègica de construcció de ciutat i generadora d’igualtat** o la **cultura com a bé comú que emana del poble**, han de ser eixos claus també. Necessitem ajuntaments que garanteixin una gestió transparent i democràtica, comunitària i de proximitat dels equipaments culturals i els espais públics.

### República compartida i sobirana

Treballarem per donar cos a l’inici de debat constituent i per **articular una República socialment justa, ecologista i feminista**. Un espai de tots i totes, garant de drets de ciutadania plens per als nostres conciutadans, vinguin d’on vinguin, parlin la llengua que parlin i tinguin la nacionalitat que tinguin.

Una **República també entesa com a punt de trobada** entre les diferents ànimes que componen el nostre projecte: aquells pels quals aquesta és el destí final i aquells pels que  aquesta és una etapa necessària per la ruptura amb el règim del 78 i, alhora, una etapa més de construcció d’una alternativa al model actual. **Una República posada al servei de la transformació social i forjada des de baix**, des de cadascun dels nostres barris i pobles, amb tot el seu teixit associatiu i social.

## Acció política supramunicipal conjunta

Les candidatures articularan un subjecte polític propi per tal de facilitar la conformació de candidatures que puguin sumar els seus resultats a nivell municipal i serveixin  de **palanca de transformació de les institucions supramunicipals** a totes aquelles localitats on aquesta realitat sigui necessària. Aquest subjecte no té per objectiu substituir cap dels actuals partits o candidatures existents. És un espai que, al contrari, **pretén facilitar l’aglutinació de forces entre diversos d’aquests subjectes ja existents** i persones o entitats diverses en un marc més ampli i flexible.

Als òrgans supramunicipals s’hi ha de denunciar l’opacitat, i fer polítiques per posar tots els seus recursos al servei de la majoria de la població dels municipis. Facilitar governs d’esquerres i transformadors, fent fora el bloc del 155 i la vella sociovergència. I tot això articulant les candidatures de base, comparteixin o no un mateix paraigües jurídic, en una coordinadora per compartir experiències i recursos alhora que per facilitar la intercooperació municipal.

## Candidatures impulsores

* Guanyem Badalona en Comú
* Guanyem Cerdanyola
* Decidim Ripollet
* Guanyem Girona
* SOM Gramenet
* Sant Adrià en Comú
* Assemblea Ciutadana de Sentmenat
* Guanyem Sant Boi
* Totes Fem Badia
* Guanyem Sitges
* Les Franqueses Imagina
* Crida Premianenca
* Acció Ciutadana de Santa  Coloma de Cervelló
* Unitat. Argentona per la República
